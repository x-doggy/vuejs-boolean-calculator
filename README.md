# Vue.js boolean calculator

A Vue.js boolean calculator. My course project for 3rd stage.

## Demo

This app available from link: [https://x-doggy.gitlab.io/vuejs-boolean-calculator](https://x-doggy.gitlab.io/vuejs-boolean-calculator)!

## Summary

The application builds parse tree and truth table from boolean formula.

![Parse tree](public/screenshots/screenshot-parse-tree.png)

![Truth table](public/screenshots/screenshot-truth-table.png)

It uses [Vue.js](https://vuejs.org/), [Vuetify](https://vuetifyjs.com/) and its components as frontend library and
[Vite](https://vitejs.dev/) as module bundler.

### Starting the Development Server

To start the development server with hot-reload, run the following command. The server will be accessible at [http://localhost:3000](http://localhost:3000):

```bash
yarn dev
```

(Repeat for npm, pnpm, and bun with respective commands.)

### Building for Production

To build your project for production, use:

```bash
yarn build
```

(Repeat for npm, pnpm, and bun with respective commands.)

Once the build process is completed, your application will be ready for deployment in a production environment.
