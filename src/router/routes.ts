import { RouteRecordRaw } from "vue-router";

export type RouteWithIcon = RouteRecordRaw & {
  name: NonNullable<RouteRecordRaw["name"]>;
  meta?: RouteRecordRaw["meta"] & { icon: string };
};

export const routes = [
  {
    path: "/tree",
    name: "tree",
    component: () => import("@/pages/tree.vue"),
    meta: { icon: "$home" },
  },
  {
    path: "/truth",
    name: "truth",
    component: () => import("@/pages/truth.vue"),
    meta: { icon: "$view" },
  },
  {
    path: "/how-to",
    name: "howTo",
    component: () => import("@/pages/how-to.vue"),
    meta: { icon: "$help" },
  },
  {
    path: "/about",
    name: "about",
    component: () => import("@/pages/about.vue"),
    meta: { icon: "$information" },
  },
] satisfies RouteWithIcon[];
