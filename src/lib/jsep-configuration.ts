import jsep from "jsep";

["-", "~", "+"].forEach((item) => jsep.removeUnaryOp(item));

[
  "==",
  "!=",
  "===",
  "!==",
  "|",
  "&",
  "^",
  ">",
  "<",
  ">=",
  "<=",
  "<<",
  ">>",
  ">>>",
  "+",
  "-",
  "*",
  "/",
  "%",
].forEach((item) => jsep.removeBinaryOp(item));

export const isExpressionBinary = (
  expression: jsep.Expression,
): expression is jsep.BinaryExpression => expression.type === "BinaryExpression";

export const isExpressionUnary = (
  expression: jsep.Expression,
): expression is jsep.UnaryExpression => expression.type === "UnaryExpression";

export const isExpressionIdentifier = (
  expression: jsep.Expression,
): expression is jsep.Identifier => expression.type === "Identifier";

export const isExpressionCompound = (
  expression: jsep.Expression,
): expression is jsep.Compound => expression.type === "Compound";

export const isExpressionArray = (
  expression: jsep.Expression,
): expression is jsep.ArrayExpression => expression.type === "ArrayExpression";

export const isExpressionCall = (
  expression: jsep.Expression,
): expression is jsep.CallExpression => expression.type === "CallExpression";

export const isExpressionConditional = (
  expression: jsep.Expression,
): expression is jsep.ConditionalExpression =>
  expression.type === "ConditionalExpression";

export const isExpressionLiteral = (
  expression: jsep.Expression,
): expression is jsep.Literal => expression.type === "Literal";

export const isExpressionMember = (
  expression: jsep.Expression,
): expression is jsep.MemberExpression => expression.type === "MemberExpression";

export const isExpressionThis = (
  expression: jsep.Expression,
): expression is jsep.ThisExpression => expression.type === "ThisExpression";
