import { TreeIdentifiersCollector } from "../tree-identifiers-collector/tree-identifiers-collector";

export abstract class TreeIdentifiersView<TSrcView, TId extends string = string> {
  constructor(
    readonly tree: TSrcView,
    readonly collector: TreeIdentifiersCollector<TSrcView>,
  ) {}

  abstract get identifiers(): Set<TId>;

  get identifiersArray(): Array<{ id: number; value: TId }> {
    return Array.from(this.identifiers.values()).map((value, index) => ({
      id: index,
      value,
    }));
  }

  get identifiersAsFunctionParameters(): string {
    return this.identifiersArray.map<TId>(({ value }) => value).join(", ");
  }

  get size(): number {
    return this.identifiers.size;
  }
}
