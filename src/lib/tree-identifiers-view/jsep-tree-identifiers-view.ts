import jsep from "jsep";
import { TreeIdentifiersView } from "@/lib/tree-identifiers-view/tree-identifiers-view";
import { TreeIdentifiersCollector } from "@/lib/tree-identifiers-collector/tree-identifiers-collector";

export class JsepTreeIdentifiersView extends TreeIdentifiersView<
  jsep.Expression,
  string
> {
  constructor(
    readonly tree: jsep.Expression,
    readonly collector: TreeIdentifiersCollector<jsep.Expression>,
  ) {
    super(tree, collector);
  }

  get identifiers(): Set<string> {
    const result = new Set<string>();
    this.collector.collect(this.tree, result);
    return result;
  }
}
