import jsep from "jsep";
import {
  isExpressionArray,
  isExpressionBinary,
  isExpressionCall,
  isExpressionCompound,
  isExpressionConditional,
  isExpressionIdentifier,
  isExpressionMember,
  isExpressionUnary,
} from "@/lib/jsep-configuration";
import { TreeIdentifiersCollector } from "@/lib/tree-identifiers-collector/tree-identifiers-collector";

export class JsepTreeIdentifiersCollector extends TreeIdentifiersCollector<jsep.Expression> {
  collect(tree: jsep.Expression, accumulator: Set<string>): void {
    if (isExpressionIdentifier(tree)) {
      accumulator.add(tree.name);
    } else if (isExpressionBinary(tree)) {
      this.collect(tree.left, accumulator);
      this.collect(tree.right, accumulator);
    } else if (isExpressionUnary(tree)) {
      this.collect(tree.argument, accumulator);
    } else if (isExpressionCompound(tree)) {
      for (const child of tree.body) {
        this.collect(child, accumulator);
      }
    } else if (isExpressionArray(tree)) {
      for (const element of tree.elements) {
        this.collect(element, accumulator);
      }
    } else if (isExpressionCall(tree)) {
      for (const argument of tree.arguments) {
        this.collect(argument, accumulator);
      }
      this.collect(tree.callee, accumulator);
    } else if (isExpressionMember(tree)) {
      this.collect(tree.property, accumulator);
      this.collect(tree.object, accumulator);
    } else if (isExpressionConditional(tree)) {
      this.collect(tree.test, accumulator);
      this.collect(tree.consequent, accumulator);
      this.collect(tree.alternate, accumulator);
    }
  }
}
