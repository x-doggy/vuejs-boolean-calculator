export abstract class TreeIdentifiersCollector<TSrcTree> {
  abstract collect(tree: TSrcTree, accumulator: Set<string>): void;
}
