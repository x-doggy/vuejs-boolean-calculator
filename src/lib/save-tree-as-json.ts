export function downloadObjectAsJson(
  exportObj: string | unknown,
  exportName: string,
): void {
  const dataStr =
    "data:text/json;charset=utf-8," +
    encodeURIComponent(
      typeof exportObj !== "string" ? JSON.stringify(exportObj) : exportObj,
    );
  const downloadAnchorNode = document.createElement("a");
  downloadAnchorNode.setAttribute("href", dataStr);
  downloadAnchorNode.setAttribute("download", `${exportName}.json`);
  document.body.appendChild(downloadAnchorNode); // required for firefox
  downloadAnchorNode.click();
  downloadAnchorNode.remove();
}
