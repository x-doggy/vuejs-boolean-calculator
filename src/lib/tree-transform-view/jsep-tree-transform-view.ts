import jsep from "jsep";
import { TreeTransformer, TreeViewItem } from "@/lib/tree-transformer/tree-transformer";
import { TreeTransformView } from "@/lib/tree-transform-view/tree-transform-view";

export class JsepTreeTransformView extends TreeTransformView<jsep.Expression> {
  constructor(
    readonly formula: string,
    readonly transformer: TreeTransformer<jsep.Expression>,
  ) {
    super(formula, transformer);
  }

  get sourceTree(): jsep.Expression {
    try {
      return jsep(this.formula);
    } catch (e) {
      return jsep("");
    }
  }

  get resultTree(): TreeViewItem[] {
    return this.transformer.transform(this.sourceTree);
  }
}
