import { TreeTransformer, TreeViewItem } from "../tree-transformer/tree-transformer";

export abstract class TreeTransformView<TSrcTree> {
  constructor(
    readonly formula: string,
    readonly transformer: TreeTransformer<TSrcTree>,
  ) {}

  abstract get sourceTree(): TSrcTree;
  abstract get resultTree(): TreeViewItem[];
}
