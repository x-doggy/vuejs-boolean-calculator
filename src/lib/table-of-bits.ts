export function buildTableOfBits(depth: number, prefix = ""): string[] {
  const buf = [] as string[];
  if (depth === 0) {
    buf.push(prefix);
    return buf;
  }
  buf.push(...buildTableOfBits(depth - 1, prefix + "0"));
  buf.push(...buildTableOfBits(depth - 1, prefix + "1"));
  return buf;
}
