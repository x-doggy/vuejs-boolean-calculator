import jsep from "jsep";
import {
  isExpressionArray,
  isExpressionBinary,
  isExpressionCall,
  isExpressionCompound,
  isExpressionConditional,
  isExpressionIdentifier,
  isExpressionLiteral,
  isExpressionMember,
  isExpressionUnary,
} from "@/lib/jsep-configuration";
import { TreeTransformer, TreeViewItem } from "@/lib/tree-transformer/tree-transformer";

export class JsepTreeTransformer extends TreeTransformer<jsep.Expression> {
  transform(tree: jsep.Expression): TreeViewItem[] {
    let counter = 0;
    const result: TreeViewItem[] = [{ id: ++counter, title: `type: ${tree.type}` }];

    if (isExpressionBinary(tree)) {
      result.push({ id: ++counter, title: `operator: ${tree.operator}` });
      result.push({
        id: ++counter,
        title: `left:`,
        children: this.transform(tree.left),
      });
      result.push({
        id: ++counter,
        title: `right:`,
        children: this.transform(tree.right),
      });
    } else if (isExpressionUnary(tree)) {
      result.push({ id: ++counter, title: `operator: ${tree.operator}` });
      result.push({ id: ++counter, title: `prefix: ${tree.prefix}` });
      result.push({
        id: ++counter,
        title: "argument:",
        children: this.transform(tree.argument),
      });
    } else if (isExpressionCompound(tree)) {
      result.push({
        id: ++counter,
        title: "body:",
        children: tree.body.flatMap((child) => this.transform(child)),
      });
    } else if (isExpressionArray(tree)) {
      result.push({
        id: ++counter,
        title: "elements:",
        children: tree.elements.flatMap((child) => this.transform(child)),
      });
    } else if (isExpressionCall(tree)) {
      result.push({
        id: ++counter,
        title: "arguments:",
        children: tree.arguments.flatMap((child) => this.transform(child)),
      });
      result.push({
        id: ++counter,
        title: "callee:",
        children: this.transform(tree.callee),
      });
    } else if (isExpressionConditional(tree)) {
      result.push({
        id: ++counter,
        title: "test",
        children: this.transform(tree.test),
      });
      result.push({
        id: ++counter,
        title: "consequent",
        children: this.transform(tree.consequent),
      });
      result.push({
        id: ++counter,
        title: "alternate",
        children: this.transform(tree.alternate),
      });
    } else if (isExpressionIdentifier(tree)) {
      result.push({ id: ++counter, title: `name: ${tree.name}` });
    } else if (isExpressionLiteral(tree)) {
      result.push({ id: ++counter, title: `value: ${tree.value}` });
    } else if (isExpressionMember(tree)) {
      result.push({
        id: ++counter,
        title: "property:",
        children: this.transform(tree.property),
      });
      result.push({ id: ++counter, title: `computed: ${tree.computed}` });
      if (tree.optional !== undefined) {
        result.push({ id: ++counter, title: `optional: ${tree.optional}` });
      }
      result.push({
        id: ++counter,
        title: "object:",
        children: this.transform(tree.object),
      });
    }

    return result;
  }
}
