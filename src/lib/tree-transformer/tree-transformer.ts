export interface TreeViewItem {
  id: number;
  title: string;
  children?: TreeViewItem[];
}

export abstract class TreeTransformer<TSrcTree> {
  abstract transform(tree: TSrcTree): TreeViewItem[];
}
