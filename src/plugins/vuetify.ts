import "vuetify/styles";
import { mdiHome, mdiViewModule, mdiHelpCircle, mdiInformation } from "@mdi/js";
import { createVuetify, type ThemeDefinition } from "vuetify";
import {
  VApp,
  VContainer,
  VLayout,
  VMain,
  VSpacer,
  VToolbar,
  VToolbarItems,
  VToolbarTitle,
  VForm,
  VAvatar,
  VIcon,
  VBtn,
  VTextField,
  VTabs,
  VTab,
} from "vuetify/components";
import { Ripple } from "vuetify/directives";
import { mdi, aliases } from "vuetify/iconsets/mdi-svg";
import { VTreeview } from "vuetify/labs/VTreeview";
import colors from "vuetify/util/colors";

const light: ThemeDefinition = {
  dark: false,
  colors: {
    primary: colors.blue.darken2,
    secondary: colors.indigo.base,
    accent: colors.deepOrange.darken3,
    error: "#FF5252",
    info: "#2196F3",
    success: "#4CAF50",
    warning: "#FFC107",
    vuelogo: "#15DFB9",
  },
};

export default createVuetify({
  theme: {
    defaultTheme: "light",
    themes: {
      light,
    },
  },
  components: {
    VApp,
    VContainer,
    VLayout,
    VMain,
    VSpacer,
    VToolbar,
    VToolbarItems,
    VToolbarTitle,
    VForm,
    VAvatar,
    VIcon,
    VBtn,
    VTextField,
    VTreeview,
    VTabs,
    VTab,
  },
  directives: {
    Ripple,
  },
  icons: {
    defaultSet: "mdi",
    aliases: {
      treeviewExpand: aliases.treeviewExpand,
      treeviewCollapse: aliases.treeviewCollapse,
      home: mdiHome,
      view: mdiViewModule,
      help: mdiHelpCircle,
      information: mdiInformation,
    },
    sets: {
      mdi,
    },
  },
});
