import jsep from "jsep";
import { computed, inject, provide, ComputedRef } from "vue";
import { useInjectFormula } from "@/composables/use-formula";
import { buildTableOfBits } from "@/lib/table-of-bits";
import { JsepTreeIdentifiersView } from "@/lib/tree-identifiers-view/jsep-tree-identifiers-view";
import { JsepTreeIdentifiersCollector } from "@/lib/tree-identifiers-collector/jsep-tree-identifiers-collector";
import { TreeIdentifiersView } from "@/lib/tree-identifiers-view/tree-identifiers-view";

export type TruthProvider<TTree> = {
  calculateExpr: (bits: string) => boolean;
  identifiersView: ComputedRef<TreeIdentifiersView<TTree>>;
  tableOfBits: ComputedRef<string[]>;
};

const truthInjectionToken = Symbol("truth");

export function useTruth(): TruthProvider<jsep.Expression> {
  const identifiersCollector = new JsepTreeIdentifiersCollector();
  const noopFalseCallback = () => false as const;

  const { formula, formulaTreeView } = useInjectFormula();

  const identifiersView = computed(
    () =>
      new JsepTreeIdentifiersView(formulaTreeView.value.sourceTree, identifiersCollector),
  );

  const tableOfBits = computed<string[]>(() =>
    buildTableOfBits(identifiersView.value.size),
  );

  const functionToCalc = computed<Function>(() => {
    try {
      return new Function(
        identifiersView.value.identifiersAsFunctionParameters,
        `return new Number(${formula.value});`,
      );
    } catch {
      return noopFalseCallback;
    }
  });

  function calculateExpr(bits: string): boolean {
    const params = bits.split("").map(Number);
    return functionToCalc.value(...params);
  }

  return { calculateExpr, identifiersView, tableOfBits };
}

export function provideTruth(): void {
  provide(truthInjectionToken, useTruth());
}

export function useInjectTruth(): TruthProvider<jsep.Expression> {
  const truthStore = inject<TruthProvider<jsep.Expression>>(truthInjectionToken);

  if (!truthStore) {
    throw new Error("Truth provider is not found");
  }

  return truthStore;
}
