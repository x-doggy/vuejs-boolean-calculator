import jsep from "jsep";
import { ComputedRef, ShallowRef, computed, inject, provide, shallowRef } from "vue";
import { JsepTreeTransformView } from "@/lib/tree-transform-view/jsep-tree-transform-view";
import { JsepTreeTransformer } from "@/lib/tree-transformer/jsep-tree-transformer";
import { TreeTransformView } from "@/lib/tree-transform-view/tree-transform-view";

export type FormulaProvider<TTree> = {
  formula: ShallowRef<string>;
  updateFormula: (s: string) => void;
  formulaTreeView: ComputedRef<TreeTransformView<TTree>>;
};

const formulaInjectionToken = Symbol("formula");

export function useFormula(): FormulaProvider<jsep.Expression> {
  const formula = shallowRef<string>("");

  const updateFormula = (newValue: string) => {
    formula.value = newValue;
  };

  const transformer = new JsepTreeTransformer();

  const formulaTreeView = computed(
    () => new JsepTreeTransformView(formula.value, transformer),
  );

  return { formula, updateFormula, formulaTreeView };
}

export function provideFormula(): void {
  provide(formulaInjectionToken, useFormula());
}

export function useInjectFormula(): FormulaProvider<jsep.Expression> {
  const formulaStore = inject<FormulaProvider<jsep.Expression>>(formulaInjectionToken);

  if (!formulaStore) {
    throw new Error("Formula provider is not found");
  }

  return formulaStore;
}
